# **Desafio Calindra Tech**

## **Descrição**

1. Receba dois ou mais endereços (ex: Av. Rio Branco, 1 Centro, Rio de Janeiro RJ,
20090003; Praca Mal. Âncora, 122 Centro, Rio de Janeiro RJ, 20021200; Rua 19 de
Fevereiro, 34 Botafogo, Rio de Janeiro RJ, 22280030 ) como parâmetros de entrada

2. Resolva a geolocalização entre os endereços utilizando a API do Google
https://developers.google.com/maps/documentation/geocoding/start

3. Após isso, com a latitude e longitude em mãos dos endereços, implementar o algoritmo de
cálculo de distância Euclidiana e aplicar em todas as combinações de endereços.

4. Retorne as distâncias calculadas entre os todos os endereços e indique os endereços
mais próximos e também os endereços mais distantes.

- - -

## Getting Started
Builda o projeto, cria o container do docker com a aplicação rodando

    npm run up

  Acessar

    http://localhost:5050/api/distances?addresses[]=Av. Rio Branco, 1 Centro, Rio de Janeiro RJ, 20090003&addresses[]=Praca Mal. Ancora, 122 Centro, Rio de Janeiro RJ, 2002120&addresses[]=Rua 19 de Fevereiro, 34 Botafogo, Rio de Janeiro RJ, 22280030&addresses[]=Avenida Paulista, 7, Sao Paulo, Sao Paulo
## Rodando os testes
Rodar testes de integração

    npm run test:integration

Rodar testes de unidade

    npm run test:unit

Rodar todos os testes com coverage

    npm run test:ci

## Documentação
    http://localhost:5050/api-docs/
## Observações
- Criei a rota com GET por questão de semantica. Dependendo das especificações do projeto final, é bem provável que eu optaria por fazê-la POST, pois esse endpoint não se beneficiará de cache já que os parametros são bem variados e se tiver uma entrada de 5 ou mais endereços, a URL ficará gigantesca.
- Como o desafio não citava um modelo específico para a saída de dados do endpoint e para não criar campos a mais em alguns registros, eu optei por trazer os resultados ordenados do mais próximo ao mais distante.
- O arquivo `main/config.env.ts` existe neste projeto por praticidade. O mais seguro seria manter versionado um .env.example com todas constantes necessárias, porém vazias, com isso não seria versionado dados sensíveis. O arquivo .env com os dados reais poderia ser adicionado no momento do deploy.