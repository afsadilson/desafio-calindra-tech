export type GeoCompareDistanceModel = {
  name: string
  distance: number
}
