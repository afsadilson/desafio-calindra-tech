export type GeocodeReverseModel = {
  name: string
  lat: string
  lng: string
}
