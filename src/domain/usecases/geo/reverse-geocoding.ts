import { GeocodeReverseModel } from '@/domain/models/geocode-reverse'

export interface ReverseGeocoding {
  exec: (address: string) => Promise<GeocodeReverseModel|null>
}
