import request from 'supertest'
import app from '@/main/config/app'

describe('Distance Routes', () => {
  describe('GET /distances', () => {
    test('Should return 400 on measure distance with one address', async () => {
      await request(app)
        .get('/api/distances?addressess=Avenida Paulista, 2')
        .send()
        .expect(400)
    })

    test('Should return 400 on measure distance with none address', async () => {
      await request(app)
        .get('/api/distances')
        .send()
        .expect(400)
    })

    test('Should return 200 on measure distance with valid parameters', async () => {
      await request(app)
        .get('/api/distances?addresses[]=Avenida Paulista, 7&addresses[]=Alameda Araguaia, 7')
        .send()
        .expect(200)
    })
  })
})
