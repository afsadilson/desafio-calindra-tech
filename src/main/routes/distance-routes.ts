/* eslint-disable @typescript-eslint/no-misused-promises */

import { Router } from 'express'
import { adaptRoute } from '@/main/adapters/express-route-adapter'
import { makeDistancesController } from '@/main/factories/controllers/distance/distances-controller-factory'

export default (router: Router): void => {
  router.get('/distances', adaptRoute(makeDistancesController()))
}
