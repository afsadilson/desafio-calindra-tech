import { ReverseGeocodingGoogleRepository } from '@/infra/google/geo/reverse-geocode-google-repository'
import { ReverseGeocoding } from '@/domain/usecases/geo/reverse-geocoding'
import { GoogleReverseGeocoding } from '@/data/usecases/geo/reverse-geocoding/google-reverse-geocoding'

export const makeGoogleReverseCoding = (): ReverseGeocoding => {
  const reverseGeocodingGoogleRepository = new ReverseGeocodingGoogleRepository()
  return new GoogleReverseGeocoding(reverseGeocodingGoogleRepository)
}
