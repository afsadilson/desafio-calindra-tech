import { Controller } from '@/presentation/protocols'
import { DistancesController } from '@/presentation/controllers/distance/distances-controller'
import { makeLogControllerDecorator } from '@/main/factories/decorators/log-controller-decorator-factory'
import { makeGoogleReverseCoding } from '@/main/factories/usecases/geo/reverse-coding/google-reverse-coding-factory'
import { EuclideanAdapter } from '@/infra/geospatial/euclidean-adapter/euclidean-adapter'

export const makeDistancesController = (): Controller => {
  const controller = new DistancesController(makeGoogleReverseCoding(), new EuclideanAdapter())
  return makeLogControllerDecorator(controller)
}
