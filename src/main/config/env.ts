export default {
  port: process.env.PORT ?? 5050,
  googleApiKey: process.env.GOOGLE_API_KEY ?? 'GOOGLE_API_KEY'
}
