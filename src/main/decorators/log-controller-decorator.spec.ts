import { ok } from '@/presentation/helpers/http/http-helper'
import { Controller, HttpRequest, HttpResponse, GeoCompareDistanceModel } from '@/presentation/protocols'
import { LogControllerDecorator } from './log-controller-decorator'

describe('LogController Decorator', () => {
  const makeController = (): Controller => {
    class ControllerStub implements Controller {
      async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
        return await Promise.resolve(ok(mockGeoCompareDistanceModel()))
      }
    }
    return new ControllerStub()
  }

  const mockRequest = (): HttpRequest => ({
    query: {
      addresses: ['address1', 'address2']
    }
  })

  const mockGeoCompareDistanceModel = (): GeoCompareDistanceModel[] => ([{
    name: 'De: address1 para address2',
    distance: 0
  }])

  type SutTypes = {
    sut: LogControllerDecorator
    controllerStub: Controller
  }

  const makeSut = (): SutTypes => {
    const controllerStub = makeController()
    const sut = new LogControllerDecorator(controllerStub)
    return {
      sut,
      controllerStub
    }
  }

  test('Should call controller handle', async () => {
    const { sut, controllerStub } = makeSut()
    const handleSpy = jest.spyOn(controllerStub, 'handle')
    await sut.handle(mockRequest())
    expect(handleSpy).toHaveBeenCalledWith(mockRequest())
  })

  test('Should return the same result of the controller', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.handle(mockRequest())
    expect(httpResponse).toEqual(ok(mockGeoCompareDistanceModel()))
  })
})
