export const distancePath = {
  get: {
    tags: ['Geospatial'],
    summary: 'API para calcular distância entre localidades',
    parameters: [{
      in: 'query',
      name: 'addresses',
      required: true,
      schema: {
        type: 'array',
        items: {
          type: 'string'
        }
      },
      example: ['Av. Rio Branco, 1 Centro, Rio de Janeiro RJ, 20090003', 'Praca Mal. Ancora, 122 Centro, Rio de Janeiro RJ, 2002120', 'Rua 19 de Fevereiro, 34 Botafogo, Rio de Janeiro RJ, 22280030', 'Avenida Paulista, 7, Sao Paulo']
    }],
    responses: {
      200: {
        description: 'Sucesso',
        content: {
          'application/json': {
            schema: {
              $ref: '#/schemas/distances'
            }
          }
        }
      },
      400: {
        $ref: '#/components/badRequest'
      },
      500: {
        $ref: '#/components/serverError'
      }
    }
  }
}
