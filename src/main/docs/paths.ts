import { distancePath } from './paths/'

export default {
  '/distances': distancePath
}
