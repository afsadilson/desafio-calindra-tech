import {
  errorSchema,
  distancesSchema,
  distanceSchema,
} from './schemas/'

export default {
  error: errorSchema,
  distances: distancesSchema,
  distance: distanceSchema
}
