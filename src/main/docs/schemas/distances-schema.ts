export const distancesSchema = {
  type: 'array',
  items: {
    $ref: '#/schemas/distance'
  }
}
