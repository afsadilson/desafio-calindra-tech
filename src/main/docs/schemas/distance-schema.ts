export const distanceSchema = {
  type: 'object',
  properties: {
    name: {
      type: 'string'
    },
    distance: {
      type: 'number'
    }
  },
  required: ['name', 'distance']
}
