import { GeocodeReverseModel } from '@/domain/models/geocode-reverse'
import { ReverseGeocoding } from '@/domain/usecases/geo/reverse-geocoding'
import axios from 'axios'
import env from '@/main/config/env'

export class ReverseGeocodingGoogleRepository implements ReverseGeocoding {
  async exec (address: string): Promise<GeocodeReverseModel> {
    const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${env.googleApiKey}`)
    return {
      name: response.data.results[0].formatted_address,
      lat: response.data.results[0].geometry.location.lat,
      lng: response.data.results[0].geometry.location.lng
    }
  }
}
