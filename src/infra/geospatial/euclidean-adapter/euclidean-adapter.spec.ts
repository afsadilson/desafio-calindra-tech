import { GeocodeReverseModel } from '@/domain/models/geocode-reverse'
import { GeoCompareDistanceModel } from '@/domain/models/geo-compare-distance'
import { EuclideanAdapter } from './euclidean-adapter'

const mockGeocodeReverseModel = (): GeocodeReverseModel => ({
  name: 'locale name',
  lat: '-23.5113',
  lng: '-46.8768'
})

const mockGeoCompareDistanceModel = (): GeoCompareDistanceModel[] => ([{
  name: 'De: locale name para locale name',
  distance: 0
}])

type SutTypes = {
  sut: EuclideanAdapter
}

const makeSut = (): SutTypes => {
  const sut = new EuclideanAdapter()

  return {
    sut
  }
}

describe('GoogleReverseGeocoding', () => {
  test('Should return GeoCompareDistanceModel on success', async () => {
    const { sut } = makeSut()
    const geoCompare = await sut.compare([
      mockGeocodeReverseModel(),
      mockGeocodeReverseModel()
    ])
    expect(geoCompare).toEqual(mockGeoCompareDistanceModel())
  })
})
