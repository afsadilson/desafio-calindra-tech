import { CompareDistance } from '@/data/protocols/geospatial/compare-distance'
import { GeocodeReverseModel } from '@/domain/models/geocode-reverse'
import { GeoCompareDistanceModel } from '@/domain/models/geo-compare-distance'

export class EuclideanAdapter implements CompareDistance {
  async compare (locations: GeocodeReverseModel[]): Promise<GeoCompareDistanceModel[]> {
    const results = [] as GeoCompareDistanceModel[]
    const KM_CONVERSOR = 111.19

    for (let i = 0; i < locations.length - 1; i++) {
      for (let j = i + 1; j < locations.length; j++) {
        results.push({
          name: `De: ${locations[i].name} para ${locations[j].name}`,
          distance: Math.sqrt(Math.pow(parseFloat(locations[i].lat) - parseFloat(locations[j].lat), 2) + Math.pow(parseFloat(locations[i].lng) - parseFloat(locations[j].lng), 2)) * KM_CONVERSOR
        })
      }
    }
    results.sort((a, b) => a.distance - b.distance)
    return results
  }
}
