import { DistancesController } from './distances-controller'
import { ReverseGeocoding, GeocodeReverseModel, CompareDistance, GeoCompareDistanceModel, HttpRequest } from './distances-controller-protocols'
import { ok, serverError } from '@/presentation/helpers/http/http-helper'

const mockRequest = (): HttpRequest => ({
  query: {
    addresses: ['address', 'other address']
  }
})

const mockGeocodeReverseModel = (): GeocodeReverseModel => ({
  name: 'locale name',
  lat: '-23.5113',
  lng: '-46.8768'
})

const mockGeoCompareDistanceModel = (): GeoCompareDistanceModel[] => ([{
  name: 'De: locale name para other locale name',
  distance: 23.21231
}, {
  name: 'De: other locale name para locale name',
  distance: 23.21231
}])

const mockReverseGeocoding = (): ReverseGeocoding => {
  class ReverseGeocodingStub implements ReverseGeocoding {
    async exec (address: string): Promise<GeocodeReverseModel|null> {
      return await Promise.resolve(mockGeocodeReverseModel())
    }
  }
  return new ReverseGeocodingStub()
}

const mockCompareDistance = (): CompareDistance => {
  class CompareDistanceStub implements CompareDistance {
    async compare (addresses: GeocodeReverseModel[]): Promise<GeoCompareDistanceModel[]> {
      return await Promise.resolve(mockGeoCompareDistanceModel())
    }
  }
  return new CompareDistanceStub()
}

type SutTypes = {
  sut: DistancesController
  reverseGeocodingStub: ReverseGeocoding
  compareDistanceStub: CompareDistance
}

const makeSut = (): SutTypes => {
  const reverseGeocodingStub = mockReverseGeocoding()
  const compareDistanceStub = mockCompareDistance()
  const sut = new DistancesController(reverseGeocodingStub, compareDistanceStub)

  return {
    sut,
    reverseGeocodingStub,
    compareDistanceStub
  }
}

describe('Distances Controller', () => {
  test('Should call ReverseGeocoding', async () => {
    const { sut, reverseGeocodingStub } = makeSut()
    const geocodingSpy = jest.spyOn(reverseGeocodingStub, 'exec')
    await sut.handle(mockRequest())
    expect(geocodingSpy).toHaveBeenCalledWith('address')
    expect(geocodingSpy).toHaveBeenCalledWith('other address')
  })

  test('Should return 500 if ReverseGeocoding throws', async () => {
    const { sut, reverseGeocodingStub } = makeSut()
    jest.spyOn(reverseGeocodingStub, 'exec').mockImplementationOnce(async () => await Promise.reject(new Error()))
    const httpResponse = await sut.handle({})
    expect(httpResponse).toEqual(serverError(new Error()))
  })

  test('Should call CompareDistance', async () => {
    const { sut, compareDistanceStub } = makeSut()
    const compareSpy = jest.spyOn(compareDistanceStub, 'compare')
    await sut.handle(mockRequest())
    expect(compareSpy).toHaveBeenCalledWith([
      mockGeocodeReverseModel(),
      mockGeocodeReverseModel()
    ])
  })

  test('Should return 500 if CompareDistance throws', async () => {
    const { sut, compareDistanceStub } = makeSut()
    jest.spyOn(compareDistanceStub, 'compare').mockImplementationOnce(async () => await Promise.reject(new Error()))
    const httpResponse = await sut.handle({})
    expect(httpResponse).toEqual(serverError(new Error()))
  })

  test('Should return 200 on success', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.handle(mockRequest())
    expect(httpResponse).toEqual(ok(mockGeoCompareDistanceModel()))
  })
})
