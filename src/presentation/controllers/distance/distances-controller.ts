import { ok, serverError, badRequest } from '@/presentation/helpers/http/http-helper'
import { InvalidParamError, MissingParamError } from '@/presentation/errors'
import { Controller, HttpRequest, HttpResponse, ReverseGeocoding, GeocodeReverseModel, CompareDistance } from './distances-controller-protocols'

export class DistancesController implements Controller {
  constructor (
    private readonly reverseGeocoding: ReverseGeocoding,
    private readonly euclidean: CompareDistance
  ) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const { addresses } = httpRequest.query
      const MIN_ADDRESSES_LENGTH = 2

      if (!addresses) return badRequest(new MissingParamError('addresses'))
      if (addresses.length < MIN_ADDRESSES_LENGTH) return badRequest(new InvalidParamError('addresses'))

      const locations: GeocodeReverseModel[] = await Promise.all(
        addresses.map(async address => await this.reverseGeocoding.exec(address))
      )
      const comparedLocations = await this.euclidean.compare(locations)

      return ok(comparedLocations)
    } catch (error) {
      return serverError(error)
    }
  }
}
