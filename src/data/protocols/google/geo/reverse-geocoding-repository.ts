import { GeocodeReverseModel } from '@/domain/models/geocode-reverse'

export interface ReverseGeocodingRepository {
  exec: (address: string) => Promise<GeocodeReverseModel>
}
