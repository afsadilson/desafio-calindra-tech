import { GeocodeReverseModel } from '@/domain/models/geocode-reverse'
import { GeoCompareDistanceModel } from '@/domain/models/geo-compare-distance'

export interface CompareDistance {
  compare: (addresses: GeocodeReverseModel[]) => Promise<GeoCompareDistanceModel[]>
}
