import { ReverseGeocodingRepository, GeocodeReverseModel, ReverseGeocoding } from './google-reverse-geocoding-protocols'

export class GoogleReverseGeocoding implements ReverseGeocoding {
  constructor (private readonly reverseGeocodingRepository: ReverseGeocodingRepository) {}

  async exec (address: string): Promise<GeocodeReverseModel> {
    return await this.reverseGeocodingRepository.exec(address)
  }
}
