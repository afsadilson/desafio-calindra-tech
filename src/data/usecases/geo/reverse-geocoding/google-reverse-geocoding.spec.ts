import { ReverseGeocodingRepository, GeocodeReverseModel } from './google-reverse-geocoding-protocols'
import { GoogleReverseGeocoding } from './google-reverse-geocoding'

const mockGeocodeReverseModel = (): GeocodeReverseModel => ({
  name: 'locale name',
  lat: '-23.5113',
  lng: '-46.8768'
})

const mockReverseGeocodingRepository = (): ReverseGeocodingRepository => {
  class ReverseGeocodingRepositoryStub implements ReverseGeocodingRepository {
    async exec (address: string): Promise<GeocodeReverseModel> {
      return await Promise.resolve(mockGeocodeReverseModel())
    }
  }
  return new ReverseGeocodingRepositoryStub()
}

type SutTypes = {
  sut: GoogleReverseGeocoding
  reverseGeocodingRepositoryStub: ReverseGeocodingRepository
}

const makeSut = (): SutTypes => {
  const reverseGeocodingRepositoryStub = mockReverseGeocodingRepository()
  const sut = new GoogleReverseGeocoding(reverseGeocodingRepositoryStub)

  return {
    sut,
    reverseGeocodingRepositoryStub
  }
}

describe('GoogleReverseGeocoding', () => {
  test('Should call ReverseGeocodingRepository', async () => {
    const { sut, reverseGeocodingRepositoryStub } = makeSut()
    const execSpy = jest.spyOn(reverseGeocodingRepositoryStub, 'exec')
    await sut.exec('any_address')
    expect(execSpy).toHaveBeenCalledWith('any_address')
  })

  test('Should return GeocodeReverse on success', async () => {
    const { sut } = makeSut()
    const geocodeReverse = await sut.exec('any_address')
    expect(geocodeReverse).toEqual(mockGeocodeReverseModel())
  })

  test('Should throw if ReverseGeocodingRepository throws', async () => {
    const { sut, reverseGeocodingRepositoryStub } = makeSut()
    jest.spyOn(reverseGeocodingRepositoryStub, 'exec').mockImplementationOnce(async () => await Promise.reject(new Error()))
    const promise = sut.exec('any_address')
    await expect(promise).rejects.toThrow()
  })
})
